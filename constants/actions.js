export const FETCH_LEADER_BOARD = "FETCH_LEADER_BOARD";
export const SUBMIT_SCORE = "SUBMIT_SCORE";
export const SORT_LEADER_BOARD = "SORT_LEADER_BOARD";
