export const BASE_URL =
  "https://gist.githubusercontent.com/d7my11/3944162529290df6e1192822fb55dae5/raw/877b1907a520e45126563dfb80cd8e22282fc28e/leaderboard.json";

export const LEADER_BOARD_DISPLAY_COUNT = 10;
export const DISMISS_NOTIFICATION_AFTER_MS = 5000;
