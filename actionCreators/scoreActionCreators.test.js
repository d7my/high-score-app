import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import promiseMiddleware from "redux-promise-middleware";
import { FULFILLED, PENDING } from "../utils/reduxUtils";
import * as actions from "../constants/actions";
import { fetchLeaderBoard } from './scoreActionCreators'

const payload = { list: [] };

global.fetch = jest.fn(() =>
  Promise.resolve({
    status: 200,
    headers: {
      get: () => "application/json",
    },
    json: () => Promise.resolve(payload),
  })
);

const middleWares = [
  promiseMiddleware,
  thunk,
];
const mockStore = configureMockStore(middleWares);
const store = mockStore({ score: { leaders: [] } });


describe("score action creators", () => {
  beforeEach(() => {
    fetch.mockClear();
  });

  describe("when fetching leader board", () => {
    beforeEach(() => {
      store.clearActions();
    });

    it("dispatches PENDING action", async () => {
      await store.dispatch(fetchLeaderBoard());
      const action = store.getActions()[0];
      expect(action.type).toEqual(PENDING(actions.FETCH_LEADER_BOARD));
    });

    it("dispatches FULFILLED action", async () => {
      await store.dispatch(fetchLeaderBoard());
      const action = store.getActions()[1];
      expect(action.type).toEqual(FULFILLED(actions.FETCH_LEADER_BOARD));
      expect(action.payload).toEqual(payload);
    });
  });
});
