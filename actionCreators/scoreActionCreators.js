import * as actions from "../constants/actions";
import { BASE_URL } from "../constants/base";
import { paramsToQueryParams } from "../utils/url";
import { uniqueId } from "../utils/uniqueId";

const generatePromise = async (params) => {
  const queryParams = paramsToQueryParams({ ...params });
  const response = await fetch(`${BASE_URL}?${queryParams}`);

  if (response.status >= 200 && response.status < 400) {
    return response.json();
  }
};

const generateFakePromise = (params) => {
  return new Promise((resolve) =>
    resolve({
      ...params,
      id: uniqueId(),
    })
  );
};

export const fetchLeaderBoard = (params) => {
  return function (dispatch, getState) {
    return dispatch({
      type: actions.FETCH_LEADER_BOARD,
      payload: generatePromise(params),
    });
  };
};

export const submitScore = (params) => {
  return function (dispatch, getState) {
    return dispatch({
      type: actions.SUBMIT_SCORE,
      payload: generateFakePromise(params),
    });
  };
};

export const sortLeaderBoard = (sortBy) => ({
  type: actions.SORT_LEADER_BOARD,
  sortBy,
});
