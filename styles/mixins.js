export const $largerThanQuery = ($breakPoint) =>
  `(min-width: ${$breakPoint}px)`;
export const $largerThan = ($breakPoint) =>
  `@media ${$largerThanQuery($breakPoint)}`;

export const $smallerThanQuery = ($breakPoint) =>
  `(max-width: ${$breakPoint}px)`;
export const $smallerThan = ($breakPoint) =>
  `@media ${$smallerThanQuery($breakPoint)}`;

export const $betweenWidth = ($minGridBreakPoint, $maxGridBreakPoint) =>
  `@media (min-width: ${$minGridBreakPoint}px and max-width: ${$maxGridBreakPoint}px)`;
