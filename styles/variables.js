// Grid breakpoints
export const $xSmall = 576;
export const $small = 769;
export const $medium = 992;
export const $large = 1201;
export const $xLarge = 1800;

// Sizes
const xs = 2;
const sm = 4;
const base = 8;
const lg = 16;
const xl = 24;
const xxl = 32;
const xxxl = 40;
const xxxxl = 48;

export const $colors = {
  primary: {
    100: "#248bd2",
    200: "#1f7dbd",
    300: "#1a6294",
  },
};

export const $primaryBorderRadius = "3px";

export const $sizes = {
  numbers: {
    xs,
    sm,
    base,
    lg,
    xl,
    xxl,
    xxxl,
    xxxxl,
  },
  xs: `${xs}px`,
  sm: `${sm}px`,
  base: `${base}px`,
  lg: `${lg}px`,
  xl: `${xl}px`,
  xxl: `${xxl}px`,
  xxxl: `${xxxl}px`,
  xxxxl: `${xxxxl}px`,
};
