import { paramsToQueryParams } from "./url";

describe("paramsToQueryParams", () => {
  describe("when called with no params", () => {
    it("returns an empty string", () => {
      expect(paramsToQueryParams()).toEqual("");
      expect(paramsToQueryParams(null)).toEqual("");
    });
  });

  describe("when called with single param", () => {
    it("returns query params", () => {
      expect(
        paramsToQueryParams({
          score: 10,
        })
      ).toEqual("score=10");
    });
  });

  describe("when called with multiple params", () => {
    it("returns query params", () => {
      expect(
        paramsToQueryParams({
          score: 10,
          clicks: 3,
        })
      ).toEqual("score=10&clicks=3");
    });
  });
});
