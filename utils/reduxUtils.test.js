import { PENDING, FULFILLED, REJECTED } from "./reduxUtils";

describe("PENDING", () => {
  describe("when receiving an action name", () => {
    it("returns correct action name", () => {
      expect(PENDING("SUBMIT")).toEqual("SUBMIT_PENDING");
    });
  });
});

describe("FULFILLED", () => {
  describe("when receiving an action name", () => {
    it("returns correct action name", () => {
      expect(FULFILLED("SUBMIT")).toEqual("SUBMIT_FULFILLED");
    });
  });
});

describe("REJECTED", () => {
  describe("when receiving an action name", () => {
    it("returns correct action name", () => {
      expect(REJECTED("SUBMIT")).toEqual("SUBMIT_REJECTED");
    });
  });
});
