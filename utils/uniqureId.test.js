import { uniqueId } from "./uniqueId";

describe("uniqueId", () => {
  describe("when calling uniqueId", () => {
    it("returns a string", () => {
      const actual = typeof uniqueId();
      expect(actual).toEqual("string");
    });
  });
});
