export const paramsToQueryParams = (params) => {
  if (!params) return "";

  const searchParams = new URLSearchParams();

  Object.keys(params).forEach((key) => {
    if (key) searchParams.append(key, params[key]);
  });

  return searchParams.toString();
};
