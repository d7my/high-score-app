export const PENDING = (action) => `${action}_PENDING`;
export const FULFILLED = (action) => `${action}_FULFILLED`;
export const REJECTED = (action) => `${action}_REJECTED`;
