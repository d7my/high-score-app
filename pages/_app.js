import Head from "next/head";
import { Provider } from 'react-redux'
import GlobalStyle from '../components/GlobalStyle'
import Header from '../components/Header'
import { useStore } from '../store/store'

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)

  return (
    <>
      <Head>
        <link
          href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,500,600&display=swap"
          rel="stylesheet"
        />
      </Head>
      <GlobalStyle />
      <Provider store={store}>
        <>
          <Header />
          <Component {...pageProps} />
        </>
      </Provider>
    </>
  );
}
