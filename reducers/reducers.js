import { combineReducers } from "redux";
import { scoreReducer } from "./scoreReducer";

const reducers = {
  score: scoreReducer,
};

export default combineReducers(reducers);
