import * as actions from "../constants/actions";
import { FULFILLED, PENDING } from "../utils/reduxUtils";
import { LEADER_BOARD_DISPLAY_COUNT } from "../constants/base";

export const scoreInitialState = {
  leaders: {
    isLoading: false,
    list: [],
  },
};

const sortBy = (sortingKey) => (a, b) => b[sortingKey] - a[sortingKey];

export const scoreReducer = (state = scoreInitialState, action) => {
  switch (action.type) {
    case PENDING(actions.FETCH_LEADER_BOARD): {
      return {
        ...state,
        leaders: {
          ...state.leaders,
          isLoading: true,
        },
      };
    }

    case FULFILLED(actions.FETCH_LEADER_BOARD): {
      const sortByScore = sortBy("totalPoints");
      return {
        ...state,
        leaders: {
          list: action.payload.list.sort(sortByScore),
          isLoading: false,
        },
      };
    }

    case FULFILLED(actions.SUBMIT_SCORE): {
      const sortByScore = sortBy("totalPoints");
      return {
        leaders: {
          ...state.leaders,
          list: [...state.leaders.list, action.payload]
            .sort(sortByScore)
            .slice(0, LEADER_BOARD_DISPLAY_COUNT),
        },
      };
    }

    case actions.SORT_LEADER_BOARD: {
      const sortingMethod = sortBy(action.sortBy);
      return {
        ...state,
        leaders: {
          ...state.leaders,
          list: state.leaders.list.sort(sortingMethod),
        },
      };
    }

    default:
      return state;
  }
};
