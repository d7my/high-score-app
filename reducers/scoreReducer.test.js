import { scoreReducer, scoreInitialState } from "./scoreReducer";
import { PENDING, FULFILLED } from "../utils/reduxUtils";
import { FETCH_LEADER_BOARD } from "../constants/actions";

describe("score reducer", () => {
  describe("when receiving no state", () => {
    it("returns initial state", () => {
      expect(scoreReducer(undefined, {})).toEqual(scoreInitialState);
    });
  });

  describe(`when receiving ${PENDING(FETCH_LEADER_BOARD)}`, () => {
    it("marks leaders as loading", () => {
      const actual = scoreReducer(undefined, {
        type: PENDING(FETCH_LEADER_BOARD),
      });

      expect(actual.leaders.isLoading).toEqual(true);
    });
  });
  describe(`when receiving ${FULFILLED(FETCH_LEADER_BOARD)}`, () => {
    const leadersList = [
      { name: "John", totalPoints: 10, clicks: 3 },
      { name: "Jane", totalPoints: 100, clicks: 7 },
    ];

    it("marks leaders as not loading", () => {
      const actual = scoreReducer(undefined, {
        type: FULFILLED(FETCH_LEADER_BOARD),
        payload: { list: [{ name: "John", totalPoints: 10, clicks: 3 }] },
      });

      expect(actual.leaders.isLoading).toEqual(false);
    });

    it("changes leaders list", () => {
      const actual = scoreReducer(undefined, {
        type: FULFILLED(FETCH_LEADER_BOARD),
        payload: { list: leadersList },
      });

      expect(actual.leaders.list).toEqual(leadersList);
    });

    it("sorts leaders list", () => {
      const actual = scoreReducer(undefined, {
        type: FULFILLED(FETCH_LEADER_BOARD),
        payload: { list: leadersList },
      });

      const expected = [
        { name: "Jane", totalPoints: 100, clicks: 7 },
        { name: "John", totalPoints: 10, clicks: 3 },
      ];

      expected.forEach((expectedItem, index) => {
        expect(actual.leaders.list[index]).toEqual(expectedItem);
      });
    });
  });
});
