import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import CheckCircleOutlined from "@ant-design/icons/CheckCircleOutlined";
import { $sizes, $colors } from "../../../styles/variables";
import Button from "../../../components/Button";
import Notification from "../../../components/Notification";
import FormError from "../../../components/Form/FormError";
import { submitScore } from "../../../actionCreators/scoreActionCreators";

const MAXIMUM_NUMBER_OF_CLICKS = 10;

const getRandomArbitrary = (min, max) =>
  Math.floor(Math.random() * (max - min) + min);

const Form = styled.form`
  max-width: 600px;
  margin: auto;
`;

const FormContainer = styled.div`
  border: 1px solid #dee2e6;
  background: #fff;
  display: flex;
  border-radius: 3px;
  padding: 4px;
`;

const Input = styled.input`
  width: calc(100% - 16px);
  border: none;
  padding: 8px;
  border-radius: 3px;
`;

const counterTypes = {
  warning: "#da992a",
  success: "#21a77a",
};

const Counter = styled.div`
  position: absolute;
  top: 2px;
  right: 8px;
  border: 2px solid;
  border-radius: 50%;
  font-size: 13px;
  font-weight: 600;
  height: 18px;
  width: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2px;
  color: ${(props) => counterTypes[props.counterType]};
`;

const InputContainer = styled.div`
  position: relative;
  flex: 5;
`;

const ActionsContainer = styled.div`
  flex: 2;
  display: flex;
  justify-content: space-between;
`;

const isFormValid = (values) => {
  if (!values.name) {
    return false;
  }

  return true;
};

const YourPointsContainer = styled.div`
  padding: ${$sizes.base} 0;
  color: ${$colors.primary[100]};
  text-align: center;
`;

const YourPoints = (props) =>
  props.score === 0 ? (
    <YourPointsContainer>
      You don't have any points yet! hit (Generate score to start)
    </YourPointsContainer>
  ) : (
    <YourPointsContainer>You have {props.score} points</YourPointsContainer>
  );

const ScoreForm = () => {
  const dispatch = useDispatch();
  const [notification, setNotification] = useState();
  const [error, setError] = useState();
  const [username, setUser] = useState("");
  const [score, setScore] = useState(0);
  const [clickCount, setClickCount] = useState(0);
  const remainingClicks = MAXIMUM_NUMBER_OF_CLICKS - clickCount;

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    const requestBody = {
      name: username,
      totalPoints: score,
      clicks: clickCount,
    };
    const isValid = isFormValid(requestBody);
    if (isValid) {
      await dispatch(submitScore(requestBody));
      setClickCount(0);
      setNotification("Your score has been submitted successfully!");
      setError(null);
    } else {
      setError("You forgot to write your name");
    }
  };

  const handleScoreGenerate = () => {
    const scoreAdj = getRandomArbitrary(-100, 100);

    setClickCount((previousCount) => {
      if (previousCount < MAXIMUM_NUMBER_OF_CLICKS) {
        setScore((previousScore) => previousScore + scoreAdj);
        return previousCount + 1;
      }
      return previousCount;
    });
  };

  return (
    <div>
      {notification && (
        <Notification
          message={notification}
          onClose={() => setNotification(null)}
        />
      )}
      <Form onSubmit={handleFormSubmit}>
        <FormError error={error} />
        <FormContainer>
          <InputContainer>
            <Input
              value={username}
              onChange={(event) => setUser(event.target.value)}
              type="text"
              placeholder="Your name"
            />
            <Counter counterType={remainingClicks <= 3 ? "warning" : "success"}>
              {remainingClicks}
            </Counter>
          </InputContainer>
          <ActionsContainer>
            <Button type="button" primary onClick={handleScoreGenerate}>
              Generate a score
            </Button>
            <Button>Go!</Button>
          </ActionsContainer>
        </FormContainer>
        <YourPoints score={score} />
      </Form>
    </div>
  );
};
export default ScoreForm;
