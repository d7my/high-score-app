import { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import Table, { TableHead, TableBody, Th } from "../../../components/Table";
import { ComponentLoader } from "../../../components/Loader";
import { $sizes } from "../../../styles/variables";
import {
  fetchLeaderBoard,
  sortLeaderBoard,
} from "../../../actionCreators/scoreActionCreators";

const LeaderBoardContainer = styled.div`
  max-width: 700px;
  margin: auto;
  padding-top: ${$sizes.xxxxl};
`;

const pointsPerClick = (totalPoints, clicks) =>
  parseInt(totalPoints / clicks) || 0;

const LeaderBoard = () => {
  const dispatch = useDispatch();
  const { leaders, isLoading } = useSelector((state) => ({
    leaders: state.score.leaders.list,
    isLoading: state.score.leaders.isLoading,
  }));

  const onSortClick = (sortBy) => () => dispatch(sortLeaderBoard(sortBy));

  useEffect(() => {
    dispatch(fetchLeaderBoard());
  }, []);

  return (
    <LeaderBoardContainer>
      {isLoading ? (
        <ComponentLoader />
      ) : (
        <Table>
          <TableHead>
            <tr>
              <Th>#</Th>
              <Th>Username</Th>
              <Th isSortedByDefault onClick={onSortClick("totalPoints")}>
                Points
              </Th>
              <Th onClick={onSortClick("clicks")}>Clicks</Th>
              <Th>Points/clicks</Th>
            </tr>
          </TableHead>
          <TableBody>
            {leaders.map((leader, index) => (
              <tr key={leader.id}>
                <td>{index + 1}</td>
                <td>{leader.name}</td>
                <td>{leader.totalPoints}</td>
                <td>{leader.clicks}</td>
                <td>{pointsPerClick(leader.totalPoints, leader.clicks)}</td>
              </tr>
            ))}
          </TableBody>
        </Table>
      )}
    </LeaderBoardContainer>
  );
};

export default LeaderBoard;
