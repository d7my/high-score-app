import styled from "styled-components";
import Page from "../../components/Layout/Page";
import LeaderBoard from "./components/LeaderBoard";
import Button from "../../components/Button";
import ScoreForm from "./components/ScoreForm";

const Home = () => (
  <Page>
    <div>
      <ScoreForm />
      <LeaderBoard />
    </div>
  </Page>
);

export default Home;
