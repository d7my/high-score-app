## HIGH SCORE

### File structure:

```sh
├── actionCreators
├── components
├── constants
├── containers
├── package.json
├── pages
├── reducers
├── store
├── styles
├── tests
└── utils
```

### App features:

- It shows high score leader board sorted by score ASC.
- It allows you to submit your score and be listed in leader board if your score qualifies.
- It allows you to sort leader board by **clicks** or **total points**.

### Development conventions:

- File names represent what is exported.

Example:
`<Button />` component has `Button.js` as a filename.

```jsx
const Button = () => (
  <button>
    Submit
  </button>
);

export default Button;
```

- File names have a `postfix` describes the content of the file to avoid looking at the path to determine whether it's an action creator file or a reducer file.

Example:

```sh
├── scoreActionCreators
└── scoreReducer
```

- Constants should be in upper-snake case.

Example:

```jsx
const BASE_URL = 'https://...';
```

- Shared components should be under `/components` directory.

- Page-specific components should be under `/Page/components` directory.



#### Tech stack choices

- **Styled components**
In a real project I wouldn't be using styled-components especially for a B2C application due to performance drawbacks, but I used it to boost my progress during development.

- **React testing library**
Because it focuses on component behavior rather than the component implementation.

- **Prettier with Eslint**
I believe eslint is a powerful linter and it has a good API. On the other hand, Prettier as a formatter focuses on who the code is written and enforces code style convention.


#### What could be improved

- Handle DESC sorting for table columns.
- Better validation for score form:
  - Different validation errors.
  - Avoid multiple component local states and replace them with component reducer.
- Increase unit tests coverage,
- Write integration tests.
- Implement better transitions.

***

## Questions:

> If you'd like, rather than limiting the number of clicks to 10 and preventing additional button clicks, reset the points counter to 0 on every 10th click of the button. Provide a brief explanation in the comments detailing the pros and cons of each restriction, in terms of usability and user experience.

In one hand, you would save the user an additional click but on the other hand, it's a bit surprising from a user perspective. A new user could generate scores so fast and they get surprised by the automated action.
I choose to keep the **GO!** button to avoid that. Also, a user could submit their score before reaching to the 10th click, so I choose to make it simple with a simple flow.

One more thing -I'm not strong on this-, if I reached the 10th click with a very high score, I would feel in control by pressing the **GO!** button.

> What API parameters would you request be made available, to optimize data processing on the front end? Assume those parameters are made available to you, and incorporate them into your code.

- `sortBy` parameter, to avoid sorting on FE, instead of of dispatching an action to sort, it should've been part of `fetchLeaderBoard` action creator

> How might this be hacked?

POST request to submit your points could be done by hand, and you could submit a custom score with minimum clicks to rank higher in the leader board.
