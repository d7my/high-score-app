import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Notification from "./Notification";

jest.useFakeTimers();

const baseProps = {
  message: "You're doing amazing!",
};

const renderComponent = (props = baseProps) =>
  render(<Notification {...props} />);

describe("<Notification />", () => {
  describe("when rendering <Notification /> component", () => {
    beforeEach(() => {
      renderComponent();
    });
    it("renders notification", () => {
      const notificationElm = screen.getByTestId("notification");
      expect(notificationElm).toBeInTheDocument();
    });

    it("renders notification message", () => {
      const notificationElm = screen.getByTestId("notification");
      expect(notificationElm.innerHTML).toEqual(baseProps.message);
    });

    it("closes notification after dismiss time passed", () => {
      const closeHandler = jest.fn();
      renderComponent({ onClose: closeHandler });
      jest.advanceTimersByTime(5000);
      expect(closeHandler).toHaveBeenCalledTimes(1);
    });
  });
});
