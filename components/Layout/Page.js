import Head from "next/head";
import styled from "styled-components";
import { $sizes } from "../../styles/variables";

const PageContainer = styled.section`
  padding: ${$sizes.xl};
`;

const Page = (props) => (
  <PageContainer>
    <Head>
      <title>{props.title}</title>
    </Head>
    {props.children}
  </PageContainer>
);

export default Page;
