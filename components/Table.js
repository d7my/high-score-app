import { useRef } from "react";
import styled, { css } from "styled-components";
import DownOutlined from "@ant-design/icons/DownOutlined";

import { $sizes } from "../styles/variables";

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
  text-align: left;
`;

export const TableHead = styled.thead`
  & > tr > th {
    border-bottom: 2px solid #dee2e6;
    padding: ${$sizes.lg};
  }
`;

export const TableBody = styled.tbody`
  & > tr > td {
    padding: ${$sizes.lg};
  }

  & > tr {
    border-bottom: 1px solid #dee2e6;

    &:last-child {
      border-bottom: none;
    }
  }

  & > tr:hover {
    background-color: rgba(0, 0, 0, 0.075);
  }
`;

const _TH = styled.th`
  ${(props) =>
    props.hasClickHandler
      ? css`
          cursor: pointer;

          &:hover,
          &:active {
            background-color: rgba(0, 0, 0, 0.075);
          }
        `
      : ""}
`;

const THContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const IconContainer = styled.span`
  transition: transform 0.3s ease;

  ${(props) =>
    props.isSortedByDefault
      ? css`
          transform: rotate(180deg);
        `
      : ""}
`;

export const Th = (props) => {
  const IconRef = useRef(null);
  return (
    <_TH
      onClick={() => {
        const currentIcon = IconRef.current;
        if (!props.onClick || !currentIcon) return;

        props.onClick();
        currentIcon.style.transform = currentIcon.style.transform
          ? null
          : "rotate(180deg)";
      }}
      hasClickHandler={!!props.onClick}
    >
      <THContainer>
        {props.children}
        {props.onClick && (
          <IconContainer
            isSortedByDefault={props.isSortedByDefault}
            ref={IconRef}
          >
            <DownOutlined />
          </IconContainer>
        )}
      </THContainer>
    </_TH>
  );
};

export default Table;
