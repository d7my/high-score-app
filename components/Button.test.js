import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Button from "./Button";

const baseProps = {
  children: "Click me!",
};

const renderComponent = (props = baseProps) => render(<Button {...props} />);

describe("<Button />", () => {
  describe("when rendering <Button /> component", () => {
    beforeEach(() => {
      renderComponent();
    });
    it("renders button", () => {
      const buttonElm = screen.getByTestId("button");
      expect(buttonElm).toBeInTheDocument();
    });

    it("has default type", () => {
      const buttonElm = screen.getByTestId("button");
      expect(buttonElm.getAttribute("type")).toEqual("submit");
    });

    it("renders button children", () => {
      const buttonElm = screen.getByTestId("button");
      expect(buttonElm.innerHTML).toEqual(baseProps.children);
    });
  });

  describe("when rendering with node children", () => {
    beforeEach(() => {
      const ButtonContent = () => <span>Click me</span>;
      renderComponent({ children: <ButtonContent /> });
    });

    it("renders button", () => {
      const buttonElm = screen.getByTestId("button");
      expect(buttonElm).toBeInTheDocument();
    });

    it("renders button children", () => {
      const buttonElm = screen.getByTestId("button");
      expect(buttonElm.innerHTML).toEqual("<span>Click me</span>");
    });
  });

  describe("when rendering with a click handler", () => {
    const clickHandler = jest.fn();

    beforeEach(() => {
      renderComponent({ children: "Click me", onClick: clickHandler });
    });

    it("calls click handler", () => {
      const buttonElm = screen.getByTestId("button");
      fireEvent.click(buttonElm, {});

      expect(clickHandler).toHaveBeenCalledTimes(1);
    });
  });

  describe("when rendering without a click handler", () => {
    it("doesn't throw an error", () => {
      expect(() => {
        renderComponent({ children: "Click me" });
      }).not.toThrow();
    });
  });
});
