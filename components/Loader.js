import styled, { keyframes } from "styled-components";
import { $colors } from "../styles/variables";

const rotateAnimation = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const CircularLoaderOuterContainer = styled.div`
  display: inline-flex;
  position: relative;
  width: 80px;
  height: 80px;
`;

const CircularLoaderInnerContainer = styled.div`
  border: 8px solid ${$colors.primary[100]};
  border-radius: 50%;
  width: 60px;
  height: 60px;
  border-color: ${$colors.primary[100]} transparent transparent transparent;
  animation: ${rotateAnimation} 0.8s ease-in infinite;
`;

const ComponentLoaderContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const CircularLoader = () => (
  <CircularLoaderOuterContainer>
    <CircularLoaderInnerContainer />
  </CircularLoaderOuterContainer>
);

const Loader = () => <CircularLoader />;

export const ComponentLoader = () => (
  <ComponentLoaderContainer>
    <Loader />
  </ComponentLoaderContainer>
);

export default Loader;
