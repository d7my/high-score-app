import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background-color: #f9f9f9;
    font-family: 'IBM Plex Sans', sans-serif;
    box-sizing: border-box;
  }

  * {
    outline: none;
  }
`;

export default GlobalStyle;
