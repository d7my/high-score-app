import styled, { css } from "styled-components";
import { $colors, $sizes, $primaryBorderRadius } from "../styles/variables";

const _Button = styled.button`
  background: ${(props) =>
    props.primary ? $colors.primary[100] : "transparent"};
  color: ${(props) => (props.primary ? "#ffffff" : $colors.primary[100])};
  border: ${(props) =>
    props.primary ? "none" : `1px solid ${$colors.primary[100]}`};
  padding: ${$sizes.base};
  border-radius: ${$primaryBorderRadius};
  transition: color 0.2s ease-in;
  font-size: 0.875rem;
  cursor: pointer;
  outline-color: ${$colors.primary[300]};

  &:hover,
  &:active {
    ${(props) =>
      props.primary
        ? css`
            background: ${$colors.primary[200]};
          `
        : css`
            border: 1px solid ${$colors.primary[200]};
            color: ${$colors.primary[200]};
          `}
  }
`;

const Button = (props) => (
  <_Button
    data-testid="button"
    primary={!!props.primary}
    onClick={props.onClick}
    type={props.type || "submit"}
  >
    {props.children}
  </_Button>
);

export default Button;
