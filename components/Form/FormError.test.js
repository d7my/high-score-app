import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import FormError from "./FormError";

const baseProps = {
  error: "This is an error message",
};

const renderComponent = (props = baseProps) => render(<FormError {...props} />);

describe("<FormError />", () => {
  describe("when rendering <FormError /> component", () => {
    beforeEach(() => {
      renderComponent();
    });
    it("renders error container", () => {
      const formErrorElm = screen.getByTestId("form-error");
      expect(formErrorElm).toBeInTheDocument();
    });

    it("renders error message", () => {
      const formErrorElm = screen.getByTestId("form-error");
      expect(formErrorElm.innerHTML).toEqual(baseProps.error);
    });
  });
});
