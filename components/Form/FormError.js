import styled, { keyframes } from "styled-components";
import { $sizes, $primaryBorderRadius } from "../../styles/variables";

const fadeIn = keyframes`
  from { opacity: 0; }
  to   { opacity: 1; }
`;

const FormErrorContainer = styled.div`
  color: #e0255e;
  padding: ${$sizes.sm};
  border-radius: ${$primaryBorderRadius};
  font-size: 0.875rem;
  height: 20px;
  animation: ${fadeIn} 2s;
`;

const FormError = (props) => (
  <FormErrorContainer data-testid="form-error">
    {props.error}
  </FormErrorContainer>
);

export default FormError;
