import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Input from "./Input";

const changeHandler = jest.fn();
const baseProps = {
  onChange: changeHandler,
  error: "This is an error message",
};

const renderComponent = (props = baseProps) => render(<Input {...props} />);

describe("<Input />", () => {
  beforeEach(() => {
    changeHandler.mockClear();
  });

  describe("when rendering <Input /> component", () => {
    beforeEach(() => {
      renderComponent();
    });

    it("renders input container", () => {
      const inputContainerElm = screen.getByTestId("input-container");
      expect(inputContainerElm).toBeInTheDocument();
    });

    it("renders input field", () => {
      const inputFieldElm = screen.getByTestId("input-field");
      expect(inputFieldElm).toBeInTheDocument();
    });
  });

  describe("when passing an input label", () => {
    const label = "Full name";
    const inputId = "name";

    beforeEach(() => {
      renderComponent({
        ...baseProps,
        label,
        id: inputId,
      });
    });

    it("renders input label", () => {
      const inputLabelElm = screen.getByTestId("input-label");
      expect(inputLabelElm).toBeInTheDocument();
      expect(inputLabelElm.innerHTML).toEqual(label);
      expect(inputLabelElm.getAttribute("for")).toEqual(inputId);
    });
  });

  describe("when passing an input placeholder", () => {
    const placeholder = "Ex: John Doe";

    beforeEach(() => {
      renderComponent({
        ...baseProps,
        placeholder,
      });
    });

    it("renders input label", () => {
      const inputFieldElm = screen.getByTestId("input-field");

      expect(inputFieldElm.getAttribute("placeholder")).toEqual(placeholder);
    });
  });

  describe("when changing input value", () => {
    beforeEach(() => {
      renderComponent();
    });

    it("calls change handler", () => {
      const inputFieldElm = screen.getByTestId("input-field");
      fireEvent.change(inputFieldElm, { target: { value: "a" } });

      expect(changeHandler).toHaveBeenCalledTimes(1);
    });
  });
});
