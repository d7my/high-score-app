import styled from "styled-components";
import { $sizes, $primaryBorderRadius } from "../../styles/variables";

const InputContainer = styled.div`
  position: relative;
  display: inline-block;
  border: 1px solid #d2d2d2;
  width: 100%;
  border-radius: ${$primaryBorderRadius};
`;

const InputField = styled.input`
  border: none;
  padding: 12px;
  border-radius: ${$primaryBorderRadius};
  width: calc(100% - 24px);
`;

const InputLabel = styled.label`
  font-size: 0.75rem;
  position: absolute;
  top: -12px;
  left: ${$sizes.base};
  background-color: #fff;
  padding: 4px;
  border-radius: ${$primaryBorderRadius};
  color: #7b7b7b;
`;

const Input = (props) => (
  <InputContainer data-testid="input-container">
    {props.label && (
      <InputLabel data-testid="input-label" htmlFor={props.id}>
        {props.label}
      </InputLabel>
    )}
    <InputField
      name={props.name}
      id={props.id}
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.onChange}
      data-testid="input-field"
    />
  </InputContainer>
);

export default Input;
