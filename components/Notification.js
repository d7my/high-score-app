import { useEffect, useRef } from "react";
import styled from "styled-components";
import { $sizes, $primaryBorderRadius } from "../styles/variables";
import { DISMISS_NOTIFICATION_AFTER_MS } from "../constants/base";

const notificationTypes = {
  success: "#21a77a",
  error: "#e0245e",
};

const NotificationContainer = styled.div`
  position: fixed;
  z-index: 10;
  top: ${$sizes.base};
  left: 50%;
  max-width: 240px;
  border-radius: ${$primaryBorderRadius};
  transform: translate(-50%, -1000px);
  transition: transform 0.5s ease;
  background: ${(props) => notificationTypes[props.notificationType]};
  color: #fff;
  font-size: 0.875rem;
  box-shadow: 0 1px 3px 0 rgba(63, 63, 68, 0.1);
  padding: ${$sizes.base} ${$sizes.lg};
`;

const Notification = (props) => {
  const notificationContainerRef = useRef(null);

  useEffect(() => {
    const currentContainer = notificationContainerRef.current;

    if (currentContainer) {
      setTimeout(() => {
        currentContainer.style.transform = "translate(-50%, 0px)";
      });

      setTimeout(() => {
        currentContainer.style.transform = "translate(-50%, -1000px)";

        props.onClose && props.onClose();
      }, props.dismissAfter || DISMISS_NOTIFICATION_AFTER_MS);
    }
  }, [notificationContainerRef.current]);

  return (
    <NotificationContainer
      data-testid="notification"
      ref={notificationContainerRef}
      notificationType={props.type || "success"}
    >
      {props.message}
    </NotificationContainer>
  );
};

export default Notification;
