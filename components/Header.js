import styled from "styled-components";
import { $sizes } from "../styles/variables";

const HeaderContainer = styled.header`
  position: relative;
  text-transform: uppercase;
  padding: ${$sizes.lg} ${$sizes.xl};

  &::after {
    position: absolute;
    content: "";
    width: 100%;
    background: red;
  }
`;

const Header = () => (
  <HeaderContainer data-testid="header">High Score</HeaderContainer>
);

export default Header;
