import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Table, { Th, TableBody, TableHead } from "./Table";

const ScoreTable = (props) => (
  <Table data-testid="table">
    <TableHead data-testid="table-head">
      <tr>
        <Th>score</Th>
        <Th>points</Th>
      </tr>
    </TableHead>
    <TableBody data-testid="table-body">
      <tr>
        <td>10</td>
        <td>1</td>
      </tr>
      <tr>
        <td>-100</td>
        <td>10</td>
      </tr>
    </TableBody>
  </Table>
);

const baseProps = {};
const renderComponent = (props = baseProps) =>
  render(<ScoreTable {...props} />);

describe("<Table />", () => {
  describe("when rendering <Table />", () => {
    beforeEach(() => {
      renderComponent();
    });

    it("renders table container", () => {
      const tableElm = screen.getByTestId("table");
      expect(tableElm).toBeInTheDocument();
    });

    it("renders table head", () => {
      const tableHeadElm = screen.getByTestId("table-head");
      expect(tableHeadElm).toBeInTheDocument();
    });

    it("renders table body", () => {
      const tableBodyElm = screen.getByTestId("table-body");
      expect(tableBodyElm).toBeInTheDocument();
    });
  });
});
